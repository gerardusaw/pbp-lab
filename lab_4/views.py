from django.shortcuts import render
from django.http import HttpResponseRedirect
from lab_2.models import Note
from lab_4.forms import NoteForm


def index(request):
    note = Note.objects.all().values()
    response = {'note': note}
    return render(request, 'lab4_index.html', response)

def note_list(request):
    note = Note.objects.all().values()
    response = {'note': note}
    return render(request, 'lab4_note_list.html', response)

def add_note(request):
    context ={}
  
    # create object of form
    form = NoteForm(request.POST or None, request.FILES or None)
    
    if request.method == "POST":
        # check if form data is valid
        if form.is_valid():
            # save the form data to model
            form.save()
            return HttpResponseRedirect('/lab-4')
    
    context['form']= form
    return render(request, "lab4_form.html", context)
# Create your views here.
