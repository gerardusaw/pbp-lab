from django import forms
from lab_2.models import Note

class NoteForm(forms.ModelForm):
    class Meta:
        model = Note
        fields = ['untuk', 'dari', 'judul', 'pesan']
    
    untuk = forms.CharField(label='Untuk', max_length=30)
    dari = forms.CharField(label='Dari', max_length=30)
    judul = forms.CharField(label='Judul', max_length=30)
    pesan = forms.CharField(label='Pesan', max_length=120)