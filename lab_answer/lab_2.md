1. Apakah perbedaan antara JSON dan XML?
2. Apakah perbedaan antara HTML dan XML?

Jawaban :

1. XML dikembangkan oleh World Wide Web Consortium sebagai format standar terbuka yang terdokumentasi dengan baik yang berisi seperangkat aturan tentang cara menyandikan dokumen dalam format yang dapat dibaca manusia dan yang dapat dibaca mesin. JSON dikembangkan oleh Douglas Crockford sebagai format file yang sederhana dan ringan untuk pertukaran data.

JSON tidak memiliki tag awal dan akhir dan sintaksisnya lebih ringan daripada XML karena berorientasi pada data dengan redundansi lebih sedikit yang menjadikannya alternatif yang ideal untuk menukar data melalui XML. XML, di sisi lain, membutuhkan lebih banyak karakter untuk mewakili data yang sama. Ini tidak seringan JSON.

Secara sederhana, XML hanyalah bahasa markup yang digunakan untuk menambahkan info tambahan ke teks biasa, sedangkan JSON adalah cara yang efisien untuk merepresentasikan data terstruktur dalam format yang dapat dibaca manusia.

2.Perbedaan XML dan HTML
Ada beberapa parameter berbeda untuk membandingkan perbedaan antara HTML dan XML. Mari lihat daftar parameter dan perbedaan antara kedua bahasa tersebut:

- XML adalah singkatan dari eXtensible Markup Language sedangkan HTML adalah singkatan dari Hypertext Markup Language.
- XML berfokus pada transfer data sedangkan HTML difokuskan pada penyajian data.
- XML didorong konten sedangkan HTML didorong oleh format.
- XML itu Case Sensitive sedangkan XML Case Insensitive
- XML menyediakan dukungan namespaces sementara HTML tidak menyediakan dukungan namespaces.
- XML strict untuk tag penutup sedangkan HTML tidak strict.
- Tag XML dapat dikembangkan sedangkan HTML memiliki tag terbatas.
- Tag XML tidak ditentukan sebelumnya sedangkan HTML memiliki tag yang telah ditentukan sebelumnya.         

Source :- https://blogs.masterweb.com/perbedaan-xml-dan-html/
        - https://id.sawakinome.com/articles/protocols--formats/difference-between-json-and-xml-3.html